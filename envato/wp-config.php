<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'envato_db');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(3m320T* q sp,w:2D:Nxt>cGc~,OW~!V@F#o(?!esKNqF6EX{-s*jL(q&Gf^jUw');
define('SECURE_AUTH_KEY',  '@D@Wg$oZ>|z/h`:Yf(n%%#mSCvw6/k%,jLoAjFC6@(yCWsBj0PMQ)Z3,T43pP}<p');
define('LOGGED_IN_KEY',    '?Ww56]M5oeh91IsPU`>`7q^hfiX)]s~:*?Q`kx#R7<Hd=^Am5MWPdxgHD+do[6[8');
define('NONCE_KEY',        'm1YOH}f@G^%u&R*-T>Dk G~C[;#Xo!@DF-|Q=e$h`h+}g.67}ibtd|^;de9y%j2j');
define('AUTH_SALT',        'Ve~g#bAHaql!u12N-hyJ` tl8|XaGv/_#x5fue8p6MDV^Ei,bVO.B;n!yZhi!1@w');
define('SECURE_AUTH_SALT', 'HqE6Y%`in`hVDA##aWhlaH]QdLpN2AsDzr9^An?Swhrmluj`7)m[zH[7-$q*.J{W');
define('LOGGED_IN_SALT',   '3:#T/1Tm7F4&r}-s(Vnz%<S%[eiN|Z[Hi%#Mcq32&(ljteclS4?-Xba?=Ao7odT ');
define('NONCE_SALT',       'FpVkD{EYfN|)<)cg/lNY(ar4 MI(;70DLfd6FT@g=naOTrNFe`YwQ3Rf/(y!3[ss');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
